using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.Data;


namespace BCC_Demo1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class APIController : ControllerBase
    {

        SqlConnection conn;
        private readonly IConfiguration _configuration;


        public APIController(IConfiguration configuration )
        {

            _configuration = configuration;


        }



        [HttpPost]
        [Route("Login")]
        public string Login(string username, string password)
        {
                try
                {
                    int id1;

                    conn = new SqlConnection(_configuration["ConnectionStrings:sql-conn"]);
                        using (conn)
                        {
                            SqlCommand cmd = new SqlCommand("sp_users", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@username", username);
                            cmd.Parameters.AddWithValue("@password", password);
                            cmd.Parameters.AddWithValue("@stmttype", "userlogin");
                            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                            DataTable dt = new DataTable();
                            adapter.Fill(dt);

                            if (!(dt != null && dt.Rows.Count > 0))
                             {
                                return "Login failure!";
                             }
                            id1 = (int)dt.Rows[0]["id"];


                            SqlCommand cmd2 = new SqlCommand("UPDATE tblUsers SET logged =1 Where id= @id", conn);
                            cmd2.Parameters.AddWithValue("@id", id1 );

                            conn.Open();
                            cmd2.ExecuteNonQuery();
                            conn.Close();
                        }
                }
                catch (Exception ex)
                {
                    return "Error:" + ex.Message;
                }

            return "Login successful!";

                
        }
        


        [HttpGet]
        [Route("GetItems")]
        public List<string>? GetItems()
        {

            try
            {
                conn = new SqlConnection(_configuration["ConnectionStrings:sql-conn"]);
                using (conn)
                {
                    SqlCommand cmd = new SqlCommand("sp_users2", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        String[] s = ((string)(dt.Rows[0]["todo"])).Split(",");

                        List<string> list = new();
                        foreach (string x in s)
                            list.Add(x);

                        return list;

                    }
                    else
                    {
                        List<String> s = new List<String>() { "No logged user." } ;
                        return s;
                    }
                }
            }
            catch (Exception ex)
            {
                List<String> s = new List<String>() { "Error:"+ex.Message };
                return s;
            }

        }

        [HttpPost]
        [Route("AddItems")]
        public String AddItem(string todoItem)
        {
            try
            {
                conn = new SqlConnection(_configuration["ConnectionStrings:sql-conn"]);
                using (conn)
                {
                    SqlCommand cmd = new SqlCommand("sp_users2", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    
                    String todoNew, role; 
                    int id1;

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        role = (string)dt.Rows[0]["role"];
                        
                        if (!(role!=null && role.Equals("admin")))
                            return "Access to this method is refused!";

                        id1 = (int)dt.Rows[0]["id"];
                        todoNew = dt.Rows[0]["todo"] + "," + todoItem;
                    }
                    else
                        return "No logged user.";

                    SqlCommand cmd2 = new SqlCommand("UPDATE tblUsers SET todo= @todoNew Where id= @id", conn);

                    cmd2.Parameters.AddWithValue("@todoNew", todoNew);
                    cmd2.Parameters.AddWithValue("@id", id1);

                    conn.Open();
                    cmd2.ExecuteNonQuery();
                    conn.Close();

                    return "Todo item added!";

                }
            }
            catch (Exception ex)
            {
                return "Error:"+ex.Message;
            }


        }



    }
}