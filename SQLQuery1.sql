--Table script--
CREATE TABLE [dbo].[tblUsers](
[id] [int] IDENTITY(1,1) NOT NULL,
[username] [nvarchar](max) NOT NULL,
[password] [nvarchar](max) NOT NULL,
[role] [nvarchar](50) NULL,
[todo] [nvarchar](max) NULL,
[logged] [int]  NULL

);


--Stored procedures--
create proc sp_users
(
@username nvarchar(50)=null, @password nvarchar(50)=null, @stmttype nvarchar(50)
)
as
begin
if(@stmttype='userlogin')
begin
select * from tblUsers where username=@username and password=@password
end;
end;

create or alter proc sp_users2
as
begin
select * from tblUsers where logged=1
end;


--inserts--
insert into [dbo].[tblUsers] (username, password, role, todo, logged) values ('pero', 'pero', 'admin', 'todo1,todo2,todo3', null);
insert into [dbo].[tblUsers] (username, password, role, todo ,logged) values ('jura', 'jura', 'user', 'todo1,todo2,todo3', null);
insert into [dbo].[tblUsers] (username, password, role, todo, logged) values ('ivo', 'ivo', 'user', 'todo1,todo2,todo3', null);


--queries & dml--

--select * from tblUsers;

--update tblUsers set logged=null where logged is not null;